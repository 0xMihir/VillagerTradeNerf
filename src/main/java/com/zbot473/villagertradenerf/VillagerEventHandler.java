package com.zbot473.villagertradenerf;

import de.tr7zw.changeme.nbtapi.NBTCompoundList;
import de.tr7zw.changeme.nbtapi.NBTEntity;
import de.tr7zw.changeme.nbtapi.NBTListCompound;
import net.minecraft.server.v1_16_R2.EntityVillager;
import net.minecraft.server.v1_16_R2.VillagerTrades;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftVillager;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTransformEvent;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.*;

import static org.bukkit.persistence.PersistentDataType.LONG;

public class VillagerEventHandler implements Listener {
    private static NamespacedKey seedKey;
    private final boolean allowCureDiscount, allowTradeChanges;

    public VillagerEventHandler(VillagerTradeNerf plugin, YamlConfiguration config) {
        this.allowTradeChanges = config.getBoolean("allow-trade-changes");
        this.allowCureDiscount = config.getBoolean("allow-cure-discount");
        seedKey = new NamespacedKey(plugin, "villagerSeed");
    }

    @EventHandler
    public void onTradeAcquired(VillagerAcquireTradeEvent event) {
        if (!allowTradeChanges) {
            Villager villager = (Villager) event.getEntity();
            if (villager.getVillagerExperience() == 0)
                event.setCancelled(true);
            if (villager.getRecipeCount() == 0) {
                Random random;
                PersistentDataContainer dataContainer = villager.getPersistentDataContainer();
                if (dataContainer.has(seedKey, LONG)) {
                    random = new Random(dataContainer.get(seedKey, LONG));
                } else {
                    long seed = villager.getUniqueId().getMostSignificantBits() %
                            villager.getWorld().getUID().getMostSignificantBits();
                    random = new Random(seed);
                    dataContainer.set(seedKey, LONG, seed);
                }
                EntityVillager entityVillager = ((CraftVillager) villager).getHandle();
                ArrayList<VillagerTrades.IMerchantRecipeOption> recipeList = new ArrayList<>(
                        Arrays.asList(
                                VillagerTrades.a.get(
                                        entityVillager.getVillagerData().getProfession())
                                        .get(1)
                        )
                );

                List<VillagerTrades.IMerchantRecipeOption> newList = new ArrayList<>();

                for (int i = 0; i < 2; i++) {
                    int randomIndex = random.nextInt(recipeList.size());
                    newList.add(recipeList.get(randomIndex));
                    recipeList.remove(randomIndex);
                }
                villager.setRecipes(Arrays.asList(new MerchantRecipe[]{
                        Objects.requireNonNull(newList.get(0).a(entityVillager, random)).asBukkit(),
                        Objects.requireNonNull(newList.get(1).a(entityVillager, random)).asBukkit()
                }));
            }
        }
    }

    @EventHandler
    public void onTransform(EntityTransformEvent event) {
        Entity original = event.getEntity();
        Entity transformed = event.getTransformedEntity();
        PersistentDataContainer originalData = original.getPersistentDataContainer();
        PersistentDataContainer transformedData = transformed.getPersistentDataContainer();
        if (originalData.has(seedKey, LONG)) {
            transformedData.set(seedKey, LONG, originalData.get(seedKey, LONG));
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        if (!allowCureDiscount) {
            Inventory inventory = event.getInventory();
            if (inventory.getType() == InventoryType.MERCHANT) {
                Villager villager = (Villager) inventory.getHolder();
                if (villager != null) {
                    NBTEntity nbtVillager = new NBTEntity(villager);
                    NBTCompoundList trades = nbtVillager.getCompound("Offers").getCompoundList("Recipes");
                    for (NBTListCompound trade : trades) {
                        if (trade.getInteger("specialPrice") < 0) {
                            trade.setInteger("specialPrice", 0);
                        }
                    }
                }
            }
        }
    }
}
