package com.zbot473.villagertradenerf;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class VillagerTradeNerf extends JavaPlugin {
    public static final String fileName = "config.yml";

    @Override
    public void onEnable() {
        File configFile = new File(getDataFolder(), fileName);
        if (!configFile.exists()) {
            saveDefaultConfig();
            getServer().getPluginManager().registerEvents(new VillagerEventHandler(this,
                    (YamlConfiguration) getConfig()), this);
        } else {
            getServer().getPluginManager().registerEvents(new VillagerEventHandler(this,
                    YamlConfiguration.loadConfiguration(configFile)), this);
        }

    }
}